﻿using System;
using UniRx;
using UnityEngine;
using _Scripts;
using _Scripts.Data;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private WeaponShooter _shooter;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private Transform _transform;
    [SerializeField] private Health _health;
    [SerializeField] private SpriteRenderer _spaceShip;
    [SerializeField] private SpriteRenderer _turret;

    [HideInInspector] public WarningTriangle WarningTrangle;

    public PlayerId PlayerId { get; private set; }
    
    public Transform Transform
    {
        get { return _transform; }
    }
    
    public Health Health
    {
        get { return _health; }
    }

    private PlayerInput _input
    {
        get { return GameConfig.Input(PlayerId); }
    }

    public IObservable<Unit> StartPressed
    {
        get { return _input.Start.Where(pressed => pressed).AsUnitObservable(); }
    }
    
    private SerialDisposable disposer;

    public void Setup(PlayerId id)
    {
        PlayerId = id;
        
        disposer = new SerialDisposable().AddTo(gameObject);

        _spaceShip.sprite = GameConfig.Player(id).SpaceShip;
        _turret.sprite = GameConfig.Player(id).Turret;
        
        _input.Switch.Subscribe(useFireButton =>
        {
            Debug.Log("Switch to " + useFireButton);
            if (useFireButton)
            {
                ActivateFireButton();
            }
            else
            {
                DeactivateFireButton();
            }
        }).AddTo(gameObject);
    }

    private void ActivateFireButton()
    {
        disposer.Disposable = _input.Attack.Subscribe(Shoot);
    }

    private void DeactivateFireButton()
    {
        disposer.Disposable = _input.Horizontal
            .CombineLatest(_input.Vertical, ShouldShoot)
            .DistinctUntilChanged().Subscribe(Shoot);
    }

    private void Shoot(bool start)
    {
        _shooter.RequestShooting(new ShootRequest
        {
            StartShooting = start, Rigidbody = _rigidbody, Tint = GameConfig.Player(PlayerId).Color, Player = PlayerId
        });
    }

    private bool ShouldShoot(float horizontal, float vertical)
    {
        return Math.Abs(horizontal) > .5 || Math.Abs(vertical) > .5;
    }

    public void ActivateWeapon(Weapon newWeapon)
    {
        _shooter.ChangeWeapon(newWeapon);
    }
    
    public void DeactivateWeapon(Weapon oldWeapon)
    {
        if (_shooter.ActiveWeapon.Value == oldWeapon)
            _shooter.ChangeWeapon(GameConfig.Instance.DefaultPlayerWeapon);
    }
}