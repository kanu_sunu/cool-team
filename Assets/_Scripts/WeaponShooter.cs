﻿using System;
using System.Security.Policy;
using UniRx;
using UnityEngine;
using _Scripts.Audio;
using _Scripts.Data;

namespace _Scripts
{
    public class ShootRequest
    {
        public bool StartShooting;
        public Rigidbody2D Rigidbody;
        public Color? Tint;
        public PlayerId? Player;
    }

    public class WeaponShooter : MonoBehaviour
    {
        [SerializeField] private Transform _bulletSpawnPoint;
        [SerializeField] private Transform _weapon;
        [SerializeField] private ActorType _actor;
        [SerializeField] private Weapon _defaultWeapon;
        [SerializeField] private RandomAudioPlayer _shotRandomAudioPlayer;
        [SerializeField] private AudioPlayer _weaponEquipAudioPlayer;

        public readonly ReactiveProperty<Weapon> Weapon = new ReactiveProperty<Weapon>();
        private readonly SerialDisposable _shootingDisposer = new SerialDisposable();
        
        public IReadOnlyReactiveProperty<Weapon> ActiveWeapon { get { return Weapon; } }

        private ShootRequest _activeShootRequest;
        
        public void RequestShooting(ShootRequest request)
        {
            if (request.StartShooting)
            {
                StartShooting(request);
            }
            else
            {
                StopShooting();
            }
        }

        public void ShootTowards(Vector3 direction)
        {
            _weapon.up = direction;
        }

        private void Start()
        {
            Weapon.Value = _defaultWeapon ? _defaultWeapon : GameConfig.Instance.DefaultPlayerWeapon;

            SetupSfx();
        }

        private void SetupSfx()
        {
            if (_shotRandomAudioPlayer != null)
            {
                Weapon.Subscribe(weapon =>
                {
                    _shotRandomAudioPlayer.ClearClips();
                    weapon.ShotAudioClips.ForEach(_shotRandomAudioPlayer.AddClip);
                });
            }

            if (_weaponEquipAudioPlayer != null)
            {
                Weapon.Subscribe(weapon =>
                {
                    _weaponEquipAudioPlayer.SetClip(weapon.EquipAudioClip);
                    _weaponEquipAudioPlayer.Play();
                });
            }
        }

        private void StartShooting(ShootRequest request)
        {
            Shoot(request);

            if (Weapon.Value.ShootingInterval > 0)
            {
                _shootingDisposer.Disposable = Observable.Interval(TimeSpan.FromSeconds(Weapon.Value.ShootingInterval))
                    .Subscribe(_ => Shoot(request)).AddTo(gameObject);
                _activeShootRequest = request;
            }
        }

        private void StopShooting()
        {
            if (_shootingDisposer.Disposable != null)
                _shootingDisposer.Disposable.Dispose();
            _activeShootRequest = null;
        }

        private void Shoot(ShootRequest request)
        {
            var bullet = Instantiate(Weapon.Value.BulletPrefab, _bulletSpawnPoint.position, Quaternion.identity);
            bullet.SetVelocity(_weapon.up * Weapon.Value.BulletVelocity);
            bullet.SetSprite(Weapon.Value.BulletSprite, request.Tint ?? Weapon.Value.BulletTint);
            bullet.SetDamage(Weapon.Value.Damage, _actor, request.Player);

            if (Vector2.Dot(request.Rigidbody.velocity, -_weapon.up) < 0)
            {
                request.Rigidbody.AddForce(-_weapon.up * 2 * Weapon.Value.PushBackStrength);
            }
            else
            {
                request.Rigidbody.AddForce(-_weapon.up * Weapon.Value.PushBackStrength);
            }

            if (_shotRandomAudioPlayer != null)
            {
                _shotRandomAudioPlayer.Play();
            }
        }

        public void ChangeWeapon(Weapon newWeapon)
        {
            Weapon.Value = newWeapon;

            if (_activeShootRequest != null)
            {
                _shootingDisposer.Disposable.Dispose();
                RequestShooting(_activeShootRequest);
            }
        }
    }
}