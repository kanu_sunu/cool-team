using System;
using UniRx;
using UnityEngine;

namespace _Scripts
{
    public class DeathExplosion : MonoBehaviour
    {
        [SerializeField] private GameObject _explosionPrefab;
        [SerializeField] private float _animationDuration = 1;
        [SerializeField] private Health _health;
        
        private void Awake()
        {
            _health.IsAlive.Where(alive => !alive).Subscribe(_ => ShowExplosion()).AddTo(this);
        }

        private void ShowExplosion()
        {
            var explosion = Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            Observable.Timer(TimeSpan.FromSeconds(_animationDuration)).Subscribe(_ => Destroy(explosion));
        }
    }
}