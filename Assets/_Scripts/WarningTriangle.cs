﻿using UnityEngine;
using UnityEngine.UI;

public class WarningTriangle : MonoBehaviour
{

    [SerializeField] private Image triangle;
    [Space]
    [SerializeField] private float idleTime;
    [SerializeField] private float fullToZeroAlphaTime;

    private bool isInZone = false;

    private Color triangleColor;

    public void Enable()
    {
        if (triangle.enabled) return;

        triangle.enabled = true;
    }

    public void Disable()
    {
        if (!triangle.enabled) return;

        triangle.enabled = false;
    }

    public void EnterZone()
    {
        if (isInZone) return;

        isInZone = true;
        StartCoroutine(Blink());
    }

    public void ExitZone()
    {
        if (!isInZone) return;

        isInZone = false;
        SetAlpha(0);
        StopAllCoroutines();
    }

    private System.Collections.IEnumerator Blink()
    {
        float timer = 0;
        
        while (true)
        {
            yield return null;
            timer += Time.deltaTime;
            if (timer > (idleTime + 2 * fullToZeroAlphaTime))
            {
                timer -= (idleTime + 2 * fullToZeroAlphaTime);
            }

            SetAlpha(timer);
        }
    }

    private void SetAlpha(float timer)
    {
        if (timer < idleTime)
        {
            triangle.color = GetColor(1);
        }
        else if (timer < idleTime + fullToZeroAlphaTime)
        {
            timer -= idleTime;

            Color color = GetColor(1 - (timer / fullToZeroAlphaTime));
            triangle.color = color;
        }
        else
        {
            timer -= (idleTime + fullToZeroAlphaTime);

            Color color = GetColor(timer / fullToZeroAlphaTime);
            triangle.color = color;
        }
    }

    private Color GetColor(float alpha)
    {
        return new Color(triangleColor.r, triangleColor.g, triangleColor.b, alpha);
    }
    
    private void Awake()
    {
        triangleColor = triangle.color;
        Debug.Log("color: " + triangleColor);
    }

}