﻿using UnityEngine;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private RectTransform _fillMask;
    [SerializeField] private RectTransform _fill;
    private float _value;

    public float Value
    {
        get { return _value; }
        set
        {
            _value = value;
            _fillMask.localScale = new Vector3(Mathf.Clamp01(value), 1f, 1f);
            _fill.localScale = new Vector3(Mathf.Clamp01(1f / value), 1f, 1f);
        }
    }
}