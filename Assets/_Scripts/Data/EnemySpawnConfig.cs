using System;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.AI;

namespace _Scripts.Data
{
    [CreateAssetMenu(fileName = "EnemySpawnConfig", menuName = "Data/EnemySpawnConfig")]
    public class EnemySpawnConfig : ScriptableObject
    {
        [Serializable]
        public class EnemySpawningConfig
        {
            public Enemy Enemy;
            public int Ratio;
            public int MaxAmount;
            public float ShieldProbability;
        }

        [SerializeField] private List<EnemySpawningConfig> _enemies;
        [SerializeField] private float _initialSpawnDelay = 5;
        [SerializeField] private float _spawnInterval = 1;
        [SerializeField] private int _randomSpawnRadius = 2;
        [SerializeField] private int _randomSpawnRotation = 10;
        
        public List<EnemySpawningConfig> Enemies
        {
            get { return _enemies; }
        }

        public float InitialSpawnDelay
        {
            get { return _initialSpawnDelay; }
        }

        public float SpawnInterval
        {
            get { return _spawnInterval; }
        }

        public int RandomSpawnRadius
        {
            get { return _randomSpawnRadius; }
        }

        public int RandomSpawnRotation
        {
            get { return _randomSpawnRotation; }
        }
    }
}