using System;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.Pickups;

namespace _Scripts.Data
{
    public class GameConfig : Singleton<GameConfig>
    {
        [Serializable]
        public class PlayerConfig
        {
            public PlayerId PlayerId;
            public PlayerInput Input;
            public Color Color;
            public Sprite SpaceShip;
            public Sprite Turret;
            public Sprite Shield;
        }
        
        [Header("Player Settings")]
        [SerializeField] private Weapon _defaultPlayerWeapon;
        public Weapon DefaultPlayerWeapon
        {
            get { return _defaultPlayerWeapon; }
        }

        [HideInInspector]
        public List<Player> Players = new List<Player>();

        [SerializeField] private List<PlayerConfig> _configs;

        public static PlayerInput Input(PlayerId playerId)
        {
            return Instance._configs.Find(config => config.PlayerId == playerId).Input;
        }

        public static PlayerConfig Player(PlayerId playerId)
        {
            return Instance._configs.Find(config => config.PlayerId == playerId);
        }

        [Header("Camera")]
        public Camera Camera;

        [Header("Session Settings")]
        public SessionController SessionController;
        public HighScoreData HighScore;

        public static bool Coop;

        [Header("Prefabs")] 
        [SerializeField] private PowerUpPickup _powerUpPickupPrefab;
        public PowerUpPickup PowerUpPickupPrefab { get { return _powerUpPickupPrefab; } }
    }
}