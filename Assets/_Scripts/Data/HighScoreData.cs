using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace _Scripts.Data
{
    public class HighScoreData : MonoBehaviour
    {
        public class Entry
        {
            public string Name;
            public long DurationInSeconds;
        }

        private readonly List<Entry> _entries = new List<Entry>();
        private const string HighscoreKey = "HighScores";

        public void Start()
        {
            var entriesString = PlayerPrefs.GetString(HighscoreKey, "");
            if (entriesString == "")
                return;
            var entryStrings = entriesString.Split('|').ToList();
            _entries.AddRange(entryStrings.Select(ParseEntry));
        }

        private Entry ParseEntry(string entry)
        {
            var entryParts = entry.Split(',');
            return new Entry
            {
                Name = entryParts[0],
                DurationInSeconds = long.Parse(entryParts[1])
            };
        }

        public List<Entry> Entries
        {
            get { return _entries; }
        }

        public void AddEntry(string name, long duration)
        {
            var entryString = name + "," + duration;
            var stored = PlayerPrefs.GetString(HighscoreKey, "");
            if (stored == "")
                PlayerPrefs.SetString(HighscoreKey, entryString);
            else     
                PlayerPrefs.SetString(HighscoreKey, stored + "|" + entryString);
        }

#if UNITY_EDITOR
        [MenuItem("Cool Team/Clear HighScores")]
        public static void RunInit()
        {
            PlayerPrefs.DeleteAll();
        } 
#endif
    }
}