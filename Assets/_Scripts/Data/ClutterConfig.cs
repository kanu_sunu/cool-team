﻿using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Data
{
    [CreateAssetMenu(fileName = "ClutterConfig", menuName = "Data/ClutterConfig")]
    public class ClutterConfig : ScriptableObject
    {
        public float ObjectDensity = 0.5f;
        
        public float xExtent = 100f;
        public float yExtent = 100f;

        public float ScaleMin = 0.1f;
        public float ScaleMax = 2f;

        public float RotationSpeedMax = 0.5f;

        public float MovementSpeedMax = 0.2f;

        public List<Sprite> ClutterSprites = new List<Sprite>();

        public ClutterObject ClutterPrefab;

        public float BrightnessMin = 0.3f;
        public float BrightnessMax = 0.8f;
    }
}