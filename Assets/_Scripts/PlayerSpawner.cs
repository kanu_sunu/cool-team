using UnityEngine;
using _Scripts.Data;

namespace _Scripts
{
    public class PlayerSpawner : MonoBehaviour
    {
        [SerializeField] private Player _playerPrefab;
        [SerializeField] private bool _forceCoop;
        
        private void Start()
        {
            if (_forceCoop)
                GameConfig.Coop = true;
                
            GameConfig.Instance.Players.Add(SpawnPlayer(PlayerId.One, new Vector3(GameConfig.Coop ? -1 : 0, 0, 0)));
            if (GameConfig.Coop)
                GameConfig.Instance.Players.Add(SpawnPlayer(PlayerId.Two, new Vector3(1, 0, 0)));
        }

        private Player SpawnPlayer(PlayerId id, Vector3 position)
        {
            var player = Instantiate(_playerPrefab, position, Quaternion.identity);
            player.Setup(id);
            return player;
        }
    }
}