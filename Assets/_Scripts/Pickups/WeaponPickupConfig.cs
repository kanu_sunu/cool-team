using System;
using UnityEngine;

namespace _Scripts.Pickups
{
    [CreateAssetMenu(fileName = "NewWeaponPickup", menuName = "Data/Weapon Pickup")]
    public class WeaponPickupConfig : AbstractPickupConfig
    {
        [SerializeField] private Weapon _weapon;

        public override void Activate(Player player)
        {
            player.ActivateWeapon(_weapon);
        }

        public override void Deactivate(Player player)
        {
            player.DeactivateWeapon(_weapon);
        }
    }
}