using System;
using UnityEngine;

namespace _Scripts.Pickups
{
    public abstract class AbstractPickupConfig : ScriptableObject
    {
        [SerializeField] private double _durationInSeconds;
        [SerializeField] private Sprite _pickupSprite;
        
        public TimeSpan Duration
        {
            get { return TimeSpan.FromSeconds(_durationInSeconds); }
        }

        public Sprite Sprite { get { return _pickupSprite; } }

        public abstract void Activate(Player player);
        public abstract void Deactivate(Player player);
    }
}