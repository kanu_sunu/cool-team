using UnityEngine;
using _Scripts.Data;

namespace _Scripts.Pickups
{
    [CreateAssetMenu(fileName = "NewFullHealthPickup", menuName = "Data/Full Health Pickup")]
    public class FullHealthPickupConfig : AbstractPickupConfig
    {
        public override void Activate(Player player)
        {
            GameConfig.Instance.Players.ForEach(ResetHealth);
        }

        private static void ResetHealth(Player player)
        {
            var health = player.Health;
            var missingHealth = health.MaxHealth.Value - health.CurrentHealth.Value;
            health.ChangeHealth(missingHealth);
        }

        public override void Deactivate(Player player)
        {
            // nothing to do
        }
    }
}