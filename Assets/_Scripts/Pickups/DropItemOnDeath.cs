using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using _Scripts.Data;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace _Scripts.Pickups
{
    public class DropItemOnDeath : IDisposable
    {
        private readonly IDisposable _disposable;
        
        public DropItemOnDeath(Health health, List<EnemyConfig.DropChance> dropChances, Transform spawnPoint)
        {
            _disposable = health.IsAlive.Where(alive => !alive)
                .Select(_ => SelectPickup(dropChances))
                .Where(pickup => pickup != null)
                .Subscribe(pickup => SpawnPickup(pickup, spawnPoint));
        }

        private static void SpawnPickup(AbstractPickupConfig pickupConfig, Transform spawnPoint)
        {
            var pickup = Object.Instantiate(GameConfig.Instance.PowerUpPickupPrefab, spawnPoint.position, Quaternion.identity);
            pickup.Config = pickupConfig;
        }

        private AbstractPickupConfig SelectPickup(List<EnemyConfig.DropChance> dropChances)
        {
            foreach (var dropChance in dropChances)
            {
                if (Random.value < dropChance.Chance)
                    return dropChance.Pickup;
            }
            return null;
        }

        public void Dispose()
        {
            _disposable.Dispose();
        }
    }
}