﻿using UnityEngine;
using _Scripts.Pickups;

[CreateAssetMenu(fileName = "NewHealthPickup", menuName = "Data/Health Pickup")]
public class HealthPickupConfig : AbstractPickupConfig
{
    [SerializeField] private int _healthIncrease; 
    
    public override void Activate(Player player)
    {
        player.Health.ChangeHealth(_healthIncrease);
    }

    public override void Deactivate(Player player)
    {
        // nothing to do
    }
}