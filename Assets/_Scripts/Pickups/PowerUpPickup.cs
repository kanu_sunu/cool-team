using UniRx;
using UnityEngine;
using _Scripts.Data;

namespace _Scripts.Pickups
{
    public class PowerUpPickup : MonoBehaviour
    {
        [SerializeField] private AbstractPickupConfig _config;
        [SerializeField] private SpriteRenderer _sprite;

        public AbstractPickupConfig Config
        {
            set
            {
                _config = value;
                _sprite.sprite = _config.Sprite;    
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            var player = other.GetComponent<Player>();
            if (player != null)
            {
                _config.Activate(player);
                Observable.Timer(_config.Duration)
                    .TakeWhile(_ => !GameConfig.Instance.SessionController.GameOver.Value)
                    .Subscribe(_ => _config.Deactivate(player));
                Destroy(gameObject);
            }
        }
    }
}