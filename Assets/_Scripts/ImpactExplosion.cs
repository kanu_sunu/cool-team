using System;
using UniRx;
using UnityEngine;

namespace _Scripts
{
    public class ImpactExplosion : MonoBehaviour
    {
        [SerializeField] private GameObject _explosionPrefab;
        [SerializeField] private float _animationDuration = 1;
        [SerializeField] private Bullet _bullet;
        
        private void Awake()
        {
            _bullet.Destruction.Subscribe(_ => ShowExplosion()).AddTo(_bullet.gameObject);
        }

        private void ShowExplosion()
        {
            var velocity = _bullet.Direction * .25f;
            var position = transform.position + new Vector3(velocity.x, velocity.y);
            var explosion = Instantiate(_explosionPrefab, position, Quaternion.identity);
            Observable.Timer(TimeSpan.FromSeconds(_animationDuration)).Subscribe(_ => Destroy(explosion));
        }
    }
}