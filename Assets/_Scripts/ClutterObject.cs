using System;
using UnityEngine;
using _Scripts.Data;

namespace _Scripts
{
    public class ClutterObject : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;

        private bool _isVisible = false;
        private Vector3 _lastCameraPosition;
        private Transform _cameraTransform;

        public ClutterLayer ClutterLayer;
        public float RotationSpeed = 0f;

        public Vector2 MovementSpeed = Vector2.zero;
        private float _depth;

        private void OnBecameVisible()
        {
            _isVisible = true;
        }

        private void OnBecameInvisible()
        {
            _isVisible = false;
        }

        public Sprite Sprite
        {
            set { _renderer.sprite = value; }
        }

        public float Brightness
        {
            set { _renderer.color = new Color(value, value, value, 1f); }
        }

        public bool Flip
        {
            set { transform.localScale = new Vector3(value ? -1f : 1f, 1f, 1f); }
        }

        public Vector2 Position
        {
            set { transform.localPosition = new Vector3(value.x, value.y, 0f); }
        }

        public float Scale
        {
            set { transform.localScale = transform.localScale * value; }
        }

        public float Rotation
        {
            set { transform.localEulerAngles = new Vector3(0f, 0f, value); }
        }

        public float Depth
        {
            set
            {
                _depth = value;
                _renderer.sortingOrder = (int) Mathf.Lerp(100f, 0f, _depth);
            }
        }

        public string SortingLayer
        {
            set { _renderer.sortingLayerName = value; }
        }

        private void Start()
        {
            _cameraTransform = GameConfig.Instance.Camera.transform;
            _lastCameraPosition = _cameraTransform.position;
        }

        protected void Update()
        {
            if (_isVisible)
            {
                transform.Rotate(0, 0, RotationSpeed * Time.deltaTime);
                transform.Translate(MovementSpeed.x, MovementSpeed.y, 0);
            }

            if (ClutterLayer == ClutterLayer.Background)
            {
                var cameraDisplacement = _lastCameraPosition - _cameraTransform.position;

                var translation = -cameraDisplacement * Mathf.Lerp(0.1f, 1f, _depth);
                var rotation = transform.rotation;
                transform.rotation = Quaternion.identity;
                transform.Translate(translation);
                transform.rotation = rotation;
                
                _lastCameraPosition = _cameraTransform.position;
            }
        }
    }
}