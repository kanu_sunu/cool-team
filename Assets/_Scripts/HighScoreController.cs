using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using _Scripts.Data;

namespace _Scripts
{
    public static class ObservableExtensions
    {
        public static IObservable<T> ThrottleSinceLast<T>(this IObservable<T> source, TimeSpan duration)
        {
            return source
                .Merge(source.Skip(1).Delay(duration))
                .ThrottleFirst(duration); 
        }
    }
    
    public class HighScoreController : MonoBehaviour
    {
        [SerializeField] private HighScoreEntry _above2;
        [SerializeField] private HighScoreEntry _above1;
        [SerializeField] private HighScoreEntry _player;
        [SerializeField] private HighScoreEntry _below1;
        [SerializeField] private HighScoreEntry _below2;

        [SerializeField] private GameObject _letter1;
        [SerializeField] private GameObject _letter2;
        [SerializeField] private GameObject _letter3;

        private int _activeLetter = 1;
        private readonly Dictionary<int, char> _letters = new Dictionary<int, char>
        {
            {1, 'A'},
            {2, 'A'},
            {3, 'A'}
        };
        
        private void Start()
        {
            GameConfig.Input(PlayerId.One).Horizontal
                .Merge(GameConfig.Input(PlayerId.Two).Horizontal)
                .SkipUntil(GameConfig.Instance.SessionController.GameOver)
                .Where(_ => _activeLetter <= 3)
                .Select(input => input > 0.8 ? 1 : input < -0.8 ? -1 : 0)
                .DistinctUntilChanged()
                .Where(change => change != 0)
                .Subscribe(change =>  SwitchToLetter(_activeLetter + change));
            
            GameConfig.Input(PlayerId.One).Vertical
                .Merge(GameConfig.Input(PlayerId.Two).Vertical)
                .SkipUntil(GameConfig.Instance.SessionController.GameOver)
                .Select(input => input > 0.8 ? 1 : input < -0.8 ? -1 : 0)
                .DistinctUntilChanged()
                .Where(change => change != 0)
                .Subscribe(ChangeLetter);

            GameConfig.Instance.SessionController.GameOver.Where(over => over).Take(1)
                .Subscribe(_ => Setup()).AddTo(gameObject);

            GameConfig.Instance.SessionController.GameRestart.Subscribe(_ => AddEntry()).AddTo(gameObject);
        }
        
        

        private void AddEntry()
        {
            GameConfig.Instance.HighScore.AddEntry("" + _letters[1] + _letters[2] + _letters[3],
                (long) GameConfig.Instance.SessionController.SessionTime.Value.TotalSeconds);
        }

        private void Setup()
        {
            var duration = GameConfig.Instance.SessionController.SessionTime.Value;
            var time = (long) duration.TotalSeconds;
            var entries = GameConfig.Instance.HighScore.Entries.OrderBy(entry => entry.DurationInSeconds).ToList();
            var above = entries.Where(entry => entry.DurationInSeconds > time).ToList();
            var rank = above.Count + 1;
            var below = entries.Where(entry => entry.DurationInSeconds <= time).Reverse().ToList();

            var above2 = above.FirstOrDefault();
            var above1 = above.Skip(1).FirstOrDefault();
            
            SetupEntry(_above2, above.Count > 1 ? above2 : null, rank - 2);
            SetupEntry(_above1, above.Count > 1 ? above1 : above2, rank - 1);
            
            SetupEntry(_player, new HighScoreData.Entry
            {
                Name = "AAA", DurationInSeconds = time
            }, rank);

            var below1 = below.FirstOrDefault();
            var below2 = below.Skip(1).FirstOrDefault();
            SetupEntry(_below1, below.Count > 1 ? below2 : below1, rank + 1);
            SetupEntry(_below2, below.Count > 1 ? below2 : null, rank + 2);
        }

        private void SetupEntry(HighScoreEntry view, HighScoreData.Entry entry, int rank)
        {
            if (entry == null)
            {
                view.gameObject.SetActive(false);
                return;
            }
            view.Name = entry.Name;
            view.Rank = rank;
            view.Time = TimeSpan.FromSeconds(entry.DurationInSeconds);
        }

        private void ChangeLetter(int amount)
        {
            _letters[_activeLetter] = (char) (_letters[_activeLetter] + amount);
            if (_letters[_activeLetter] > 'Z')
                _letters[_activeLetter] = 'A';
            if (_letters[_activeLetter] < 'A')
                _letters[_activeLetter] = 'Z';
            _player.Name = "" + _letters[1] + _letters[2] + _letters[3];
        }

        private void SwitchToLetter(int letter)
        {
            if (letter >= 4 || letter <= 0)
                return;

            _activeLetter = letter;
            
            switch (letter)
            {
                case 1:
                    _letter1.SetActive(true);
                    _letter2.SetActive(false);
                    _letter3.SetActive(false);
                    break;
                case 2:
                    _letter1.SetActive(false);
                    _letter2.SetActive(true);
                    _letter3.SetActive(false);
                    break;
                case 3:
                    _letter1.SetActive(false);
                    _letter2.SetActive(false);
                    _letter3.SetActive(true);
                    break;
            }
        }

    }
}