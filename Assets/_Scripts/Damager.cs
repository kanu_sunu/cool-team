﻿using UnityEngine;
using _Scripts;

public class Damager : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private ActorType actorType;
    private PlayerId? _player;

    public int Damage { get { return damage; } }
    public ActorType ActorType { get { return actorType; } }
    public PlayerId? Player { get { return _player; } }

    public void Setup(int damage, ActorType actor, PlayerId? player)
    {
        this.damage = damage;
        this.actorType = actor;
        _player = player;
    }

}