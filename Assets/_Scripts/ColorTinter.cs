﻿using _Scripts;
using UniRx;
using UnityEngine;

public class ColorTinter : MonoBehaviour
{

    [SerializeField] private Health health;
    [Space]
    [SerializeField] private Color hitColor = Color.red;
    [SerializeField] private Color healedColor = Color.green;
    [SerializeField] private float tintTime = 1;
    [SerializeField] private SpriteRenderer[] parts;

    private Color[] initialColors;

    private int currentHealth = -1;

    private void HealthChanged(int newHealth)
    {
        if (currentHealth == -1)
        {
            currentHealth = newHealth;
            return;
        }

        Color tintColor;
        if (newHealth < currentHealth)
        {
            tintColor = hitColor;
        }
        else
        {
            tintColor = healedColor;
        }

        StartTint(tintColor);

        currentHealth = newHealth;
    }
    
    private void StartTint(Color tintColor)
    {
        StopAllCoroutines();

        for (int i = 0; i < parts.Length; i++)
        {
            StartCoroutine(ChangeColor(parts[i], initialColors[i], tintColor));
        }
    }

    private System.Collections.IEnumerator ChangeColor(SpriteRenderer part, Color initialColor, Color tintColor)
    {
        float timer = 0;
        while (timer <= tintTime)
        {
            SetColor(part, tintColor, initialColor, timer / tintTime);

            yield return null;
            timer += Time.deltaTime;
        }

        SetColor(part, tintColor, initialColor, 1);
    }

    private void SetColor(SpriteRenderer renderer, Color tintColor, Color initialColor, float timer)
    {
        renderer.color = Color.Lerp(tintColor, initialColor, timer);
    }

    private void Setup()
    {
        for (int i = 0; i < initialColors.Length; i++)
        {
            initialColors[i] = parts[i].color;
        }
    }

    private void Start()
    {
        initialColors = new Color[parts.Length];

        Setup();

        health.CurrentHealth.Subscribe(HealthChanged).AddTo(gameObject);
    }

}