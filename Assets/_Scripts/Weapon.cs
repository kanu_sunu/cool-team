﻿using System.Collections.Generic;
using UnityEngine;

namespace _Scripts
{
    [CreateAssetMenu(fileName = "NewWeapon", menuName = "Data/New Weapon")]
    public class Weapon : ScriptableObject
    {
        public float ShootingInterval = 0f;
        public int Damage;
        public float PushBackStrength = 0f;
        
        public Bullet BulletPrefab;
        public Sprite BulletSprite;
        public Color BulletTint;
        public float BulletVelocity = 10;
        
        public List<AudioClip> ShotAudioClips = new List<AudioClip>();
        public AudioClip EquipAudioClip;
    }
}
