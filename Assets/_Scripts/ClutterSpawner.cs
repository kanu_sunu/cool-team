using System.Collections.Generic;
using UnityEngine;
using _Scripts.Data;

namespace _Scripts
{
    public enum ClutterLayer
    {
        Background,
        Foreground,
    }
    
    public class ClutterSpawner : MonoBehaviour
    {
        [SerializeField] private ClutterConfig _config;
        [SerializeField] private ClutterLayer _sortingLayer;

        private Dictionary<ClutterLayer, string> _sortingLayerNames = new Dictionary<ClutterLayer, string>
        {
            {ClutterLayer.Background, "BackgroundClutter"},
            {ClutterLayer.Foreground, "ForegroundClutter"}
        };

        protected void Start()
        {
            var numObjects = _config.xExtent * _config.yExtent * _config.ObjectDensity;
            for (var idx = 0; idx < numObjects; idx++)
            {
                CreateClutterObject();
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(gameObject.transform.position, new Vector3(_config.xExtent * 2, _config.yExtent * 2, 0));
        }

        private void CreateClutterObject()
        {
            var flip = Random.Range(0f, 1f) > 0.5f;

            var initialRotation = Random.Range(0f, 360f);
            var rotationSpeed = Random.Range(0f, _config.RotationSpeedMax);
            rotationSpeed = rotationSpeed * Mathf.Pow(Random.Range(0f, 1f), 2);

            var movementSpeedX = Random.Range(- _config.MovementSpeedMax, _config.MovementSpeedMax);
            var movementSpeedY = Random.Range(- _config.MovementSpeedMax, _config.MovementSpeedMax);
            var movementSpeed = new Vector2(movementSpeedX, movementSpeedY);

            var posX = Random.Range(-_config.xExtent, _config.xExtent);
            var posY = Random.Range(-_config.yExtent, _config.yExtent);
            var pos = new Vector2(posX, posY);
            
            var spriteIdx = Random.Range(0, _config.ClutterSprites.Count);
            var sprite = _config.ClutterSprites[spriteIdx];
            
            var depth = 1 - Mathf.Pow(Random.Range(0f, 1f), 2);
            var brightness = Mathf.Lerp(_config.BrightnessMax, _config.BrightnessMin, depth);
            var scale = Mathf.Lerp(_config.ScaleMax, _config.ScaleMin, depth + Random.Range(-0.2f, 0.2f));

            var go = Instantiate(_config.ClutterPrefab.gameObject, transform);
            var clutter = go.GetComponent<ClutterObject>();
            clutter.Sprite = sprite;
            clutter.Flip = flip;
            clutter.MovementSpeed = movementSpeed;
            clutter.RotationSpeed = rotationSpeed;
            clutter.Rotation = initialRotation;
            clutter.Position = pos;
            clutter.Scale = scale;
            clutter.Brightness = brightness;
            clutter.Depth = depth;
            clutter.SortingLayer = _sortingLayerNames[_sortingLayer];
            clutter.ClutterLayer = _sortingLayer;
        }
    }
}