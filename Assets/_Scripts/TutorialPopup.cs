﻿using UnityEngine;

public class TutorialPopup : MonoBehaviour
{

    public void DestroyInSeconds(float seconds)
    {
        if (seconds < 0)
        {
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject, seconds);
        }
    }
    
}