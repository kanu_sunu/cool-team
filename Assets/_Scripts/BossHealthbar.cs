﻿using _Scripts;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthbar : MonoBehaviour {

    [SerializeField] private Health health;
    [SerializeField] private Image healthBarImage;

    private void Update()
    {
        healthBarImage.fillAmount = health.CurrentHealth.Value / 200.0f;
    }
}
