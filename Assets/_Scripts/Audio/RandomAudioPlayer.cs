using System.Collections.Generic;
using UnityEngine;

namespace _Scripts.Audio
{
    public class RandomAudioPlayer : AudioPlayer
    {
        private readonly List<AudioClip> _clips = new List<AudioClip>();

        public void AddClip(AudioClip clip)
        {
            _clips.Add(clip);
        }

        public void ClearClips()
        {
            _clips.Clear();
        }

        public override void Play()
        {
            Source.clip = GetRandomClip();

            base.Play();
        }

        private AudioClip GetRandomClip()
        {
            var clip = _clips[0];
            if (_clips.Count > 1)
            {
                clip = _clips[Random.Range(0, _clips.Count)];
            }

            return clip;
        }
    }
}