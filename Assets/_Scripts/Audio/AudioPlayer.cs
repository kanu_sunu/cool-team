using UnityEngine;

namespace _Scripts.Audio
{
    public class AudioPlayer : MonoBehaviour
    {
        [Header("Settings")]
        [Range(0f, 0.2f)]
        [SerializeField] private float _randomPitch;

        [Header("References")] 
        [SerializeField] protected AudioSource Source;

        private void Awake()
        {
            SetRandomPitch();
        }

        public void SetClip(AudioClip clip)
        {
            Source.clip = clip;
        }

        public virtual void Play()
        {
            SetRandomPitch();

            if (Source.clip != null)
            {
                Source.Play();
            }
        }

        private void SetRandomPitch()
        {
            if (Mathf.Abs(_randomPitch) > float.Epsilon)
            {
                Source.pitch = 1 + Random.Range(-_randomPitch, _randomPitch);
            }
        }
    }
}