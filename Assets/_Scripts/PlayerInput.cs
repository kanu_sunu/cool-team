﻿using System;
using UniRx;
using UnityEngine;
using _Scripts;
using _Scripts.Data;

public class PlayerInput : MonoBehaviour
{
	[SerializeField] private PlayerId _playerId;
	public PlayerId PlayerId { get { return _playerId; } }
	
	private int _id { get { return _playerId == PlayerId.One ? 1 : 2; } }

	private string _horizontalAxis;
	private string _verticalAxis;
	private string _attackButton;
	private string _switchButton;
	private string _startButton;
	private string _aButton;
	
	private readonly Subject<float> _horizontal = new Subject<float>();
	public IObservable<float> Horizontal
	{
		get { return _horizontal; }
	}
	
	private readonly Subject<float> _vertical = new Subject<float>();
	public IObservable<float> Vertical
	{
		get { return _vertical; }
	}
	
	private readonly Subject<bool> _attack = new Subject<bool>();
	public IObservable<bool> Attack
	{
		get { return _attack; }
	}
	
	private readonly Subject<bool> _a = new Subject<bool>();
	public IObservable<bool> A
	{
		get { return _a; }
	}
	
	private readonly ReactiveProperty<bool> _switch = new ReactiveProperty<bool>(false);
	public IReadOnlyReactiveProperty<bool> Switch
	{
		get { return _switch; }
	}
	
	private readonly Subject<bool> _start = new Subject<bool>();
	public IObservable<bool> Start
	{
		get { return _start; }
	}
	
	private void Awake ()
	{
		_horizontalAxis = "L_XAxis_" + _id;
		_verticalAxis = "L_YAxis_" + _id;
		_attackButton = "X_" + _id;
		_switchButton = "Back_" + _id;
		_startButton = "Start_" + _id;
		_aButton = "A_" + _id;
	}
	
	private void Update ()
	{
		if (Input.GetButtonDown(_startButton))
		{
			_start.OnNext(true);
		} else if (Input.GetButtonUp(_startButton))
		{
			_start.OnNext(false);
		}
		
		var horizontal = Input.GetAxisRaw(_horizontalAxis);
		if (Math.Abs(horizontal) > float.Epsilon)
		{
			_horizontal.OnNext(horizontal);
		}
		
		var vertical = Input.GetAxisRaw(_verticalAxis);
		if (Math.Abs(vertical) > float.Epsilon)
		{
			_vertical.OnNext(-vertical);
		}

		if (Input.GetButtonDown(_attackButton))
		{
			_attack.OnNext(true);
		} else if (Input.GetButtonUp(_attackButton))
		{
			_attack.OnNext(false);
		}
		
		if (Input.GetButtonDown(_aButton))
		{
			_a.OnNext(true);
		} else if (Input.GetButtonUp(_aButton))
		{
			_a.OnNext(false);
		}
		
		if (Input.GetButtonDown(_switchButton))
		{
			_switch.Value = !_switch.Value;
		} 
	}
}
