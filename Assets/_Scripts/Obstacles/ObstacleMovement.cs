﻿using System;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{

    [SerializeField] private float speed;
    [SerializeField] private MovementPatternConfig patternConfig;

    private MovementPattern movementPattern;

    private void Awake()
    {
        movementPattern = patternConfig.GetMovementPattern();
        movementPattern.Setup(transform);
    }

    private void Update()
    {
        movementPattern.Move(transform, speed, Time.deltaTime);
    }

}
