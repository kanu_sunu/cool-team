﻿using UnityEngine;

public class CurveMovementPattern : MovementPattern
{
    private CurveMovementPatternConfig curveConfig;

    private Vector2 position;
    private Vector2 forward;
    private Vector2 right;
    private float curveTime;
    private float timer;

    public CurveMovementPattern(CurveMovementPatternConfig curveConfig)
    {
        this.curveConfig = curveConfig;
    }

    public override void Move(Transform transform, float speed, float deltaTime)
    {
        position = position + forward * speed * deltaTime;

        timer += deltaTime;
        timer = timer % curveTime;

        Vector2 updatedPosition = position + curveConfig.Curve.Evaluate(timer) * curveConfig.Amplitude * right;

        transform.up = updatedPosition - (Vector2)transform.position;

        transform.position = updatedPosition;
    }

    public override void SetMovementDirection(Vector2 forward, Vector2 right)
    {
        this.forward = forward.normalized;
        this.right = right.normalized;
    }

    public override void SetPosition(Vector2 position)
    {
        this.position = position;
        timer = 0;
    }

    public override void Setup(Transform transform)
    {
        position = transform.position;
        forward = transform.up;
        right = transform.right;
        curveTime = curveConfig.Curve.keys[curveConfig.Curve.length - 1].time;
        timer = 0;

        Move(transform, 0, 0);
    }
}