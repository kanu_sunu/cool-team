﻿using UnityEngine;

public abstract class MovementPattern
{
    
    public abstract void Setup(Transform transform);
    public abstract void SetMovementDirection(Vector2 forward, Vector2 right);
    public abstract void SetPosition(Vector2 position);
    public abstract void Move(Transform transform, float speed, float deltaTime);

}