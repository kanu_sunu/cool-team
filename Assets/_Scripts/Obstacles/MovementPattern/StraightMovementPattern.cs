﻿using UnityEngine;

public class StraightMovementPattern : MovementPattern
{
    public StraightMovementPattern(MovementPatternConfig config)
    {}

    public override void Move(Transform transform, float speed, float deltaTime)
    {
        transform.position = (Vector2)transform.position + (Vector2)transform.up * speed * deltaTime;
    }

    public override void SetMovementDirection(Vector2 direction, Vector2 right)
    {}

    public override void SetPosition(Vector2 position)
    {}

    public override void Setup(Transform transform)
    {}
}