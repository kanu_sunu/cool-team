﻿using UnityEngine;

[CreateAssetMenu(fileName = "CurveMovementPattern", menuName = "Config/Movement Patterns/Curve Pattern")]
public class CurveMovementPatternConfig : MovementPatternConfig
{
    [SerializeField] private float amplitude;
    [SerializeField] private AnimationCurve curve;

    public float Amplitude { get { return amplitude; } }
    public AnimationCurve Curve { get { return curve; } }

    public override MovementPattern GetMovementPattern()
    {
        return new CurveMovementPattern(this);
    }
}