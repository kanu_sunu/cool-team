﻿using UnityEngine;

public abstract class MovementPatternConfig : ScriptableObject
{
    public abstract MovementPattern GetMovementPattern();

}
