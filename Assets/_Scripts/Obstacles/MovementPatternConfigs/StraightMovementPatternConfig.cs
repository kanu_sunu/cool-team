﻿using UnityEngine;

[CreateAssetMenu(fileName = "StraightMovementPattern", menuName = "Config/Movement Patterns/Straight Pattern")]
public class StraightMovementPatternConfig : MovementPatternConfig
{
    public override MovementPattern GetMovementPattern()
    {
        return new StraightMovementPattern(this);
    }
}
