﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using _Scripts.Data;
using System;

public abstract class EnemyMovement : MonoBehaviour
{
    public Transform CurrentPlayer { get; private set; }

    public EnemyConfig Config
    {
        get
        {
            return config;
        }
    }

    [SerializeField] protected EnemyConfig config;
    [SerializeField] protected MovementPatternConfig patternConfig;

    protected float currentSpeed = 0;

    protected Quaternion currentRotation = Quaternion.identity;
    protected Quaternion actualRotation = Quaternion.identity;

    protected MovementPattern movementPattern;

    protected abstract void FixedUpdate();

    protected void SetupMovementPattern()
    {
        if (currentSpeed > 0) return;

        movementPattern.SetPosition(gameObject.transform.position);
    }

    protected void RotateToLookAt()
    {
        if (currentSpeed > 0) return;
        
        gameObject.transform.rotation = Quaternion.RotateTowards(actualRotation, currentRotation, config.RotationSpeed * Time.fixedDeltaTime);
    }

    protected void Rotate(Vector2 direction)
    {
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion lookRotation = Quaternion.AngleAxis(angle, Vector3.forward) * Quaternion.Euler(0, 0, -90);

        currentRotation = Quaternion.RotateTowards(currentRotation, lookRotation, config.RotationSpeed * Time.fixedDeltaTime);
        actualRotation = gameObject.transform.rotation;
        gameObject.transform.rotation = currentRotation;
    }
    
    protected void AccelerateTowardsTarget(Vector2 currentPosition, Vector2 targetPosition)
    {
        if (Vector2.Dot((targetPosition - currentPosition), transform.up) < 0)
        {
            currentSpeed -= config.SlowDownSpeed / 2;
        }

        currentSpeed += config.AccelerationSpeed;

        currentSpeed = Mathf.Clamp(currentSpeed, 0, config.MaxMovementSpeed);
    }

    protected void SlowDown()
    {
        if (currentSpeed < config.SlowDownSpeed * Time.fixedDeltaTime)
        {
            currentSpeed = 0;
        }
        else
        {
            currentSpeed -= config.SlowDownSpeed * Time.fixedDeltaTime;
        }
    }

    private System.Collections.IEnumerator UpdateTarget(List<Transform> players)
    {
        while (true)
        {
            players.RemoveAll(p => !p);
            if (players.Count == 0) 
                yield break;
            CurrentPlayer = GetClosestTarget(players);

            yield return new WaitForSeconds(config.TargetUpdateInterval);
        }
    }

    private Transform GetClosestTarget(List<Transform> targets)
    {
        Vector3 position = gameObject.transform.position;
        int currentClosestIndex = 0;
        float closestDistance = (targets[0].position - position).magnitude;

        for (int i = 1; i < targets.Count; i++)
        {
            float distance = (targets[i].position - position).magnitude;
            if (distance < closestDistance)
            {
                closestDistance = distance;
                currentClosestIndex = i;
            }
        }

        return targets[currentClosestIndex];
    }

    private void Setup(List<Transform> players)
    {
        StartCoroutine(UpdateTarget(players));
    }

    private void Start()
    {
        movementPattern = patternConfig.GetMovementPattern();
        movementPattern.Setup(transform);

        Setup(GameConfig.Instance.Players.Select(player => player.Transform).ToList());
    }

}