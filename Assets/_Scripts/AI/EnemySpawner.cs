﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UniRx;
using UnityEngine;
using _Scripts;
using _Scripts.AI;
using _Scripts.Data;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private TutorialPopup tutorial;
    [SerializeField] private float _tutorialDuration;
    [Space] 
    [SerializeField] private EnemySpawnConfig _config;
    [Space]
    [SerializeField] private Transform[] spawnAreas;
    
    private readonly Dictionary<EnemyConfig, int> _existingEnemies = new Dictionary<EnemyConfig, int>(); 
    
    private void Start()
    {
        Instantiate(tutorial, null).DestroyInSeconds(_tutorialDuration);
        StartCoroutine(SpawnEnemy());
    }

    private IEnumerator SpawnEnemy()
    {
        yield return new WaitForSeconds(_config.InitialSpawnDelay);

        while (true)
        {
            var selectedArea = GetRandomArea();
            var position = GetAreaPosition(selectedArea);
            var rotation = GetAreaRotation(selectedArea);
            SpawnEnemy(position, rotation);

            yield return new WaitForSeconds(_config.SpawnInterval);
        }
    }

    private void SpawnEnemy(Vector2 position, Quaternion rotation)
    {
        var config = GetRandomEnemyConfig();
        if (config == null)
            return;
        
        var enemy = Instantiate(config.Enemy, position, rotation);

        if (Random.value < config.ShieldProbability)
            enemy.ActivateShield(!GameConfig.Coop || Random.value < 0.5 ? PlayerId.One : PlayerId.Two);

        CountEnemy(enemy);
    }

    private void CountEnemy(Enemy enemy)
    {
        if (!_existingEnemies.ContainsKey(enemy.Config))
            _existingEnemies[enemy.Config] = 0;
        _existingEnemies[enemy.Config] = _existingEnemies[enemy.Config] + 1; 
            
        enemy.Health.IsAlive.Where(alive => !alive)
            .Subscribe(_ => _existingEnemies[enemy.Config] = _existingEnemies[enemy.Config] - 1)
            .AddTo(enemy);
    }

    private EnemySpawnConfig.EnemySpawningConfig GetRandomEnemyConfig()
    {
        var enemies = SpawnableEnemies;
        var maxRatio = enemies.Sum(enemy => enemy.Ratio);
        var randomNumber = Random.Range(0, maxRatio);
        while (enemies.Count > 0)
        {
            var ratio = enemies.First().Ratio;
            if (randomNumber <= ratio)
                return enemies.First();
            maxRatio -= ratio;
            randomNumber -= ratio;
            enemies.RemoveAt(0);
        }
        return null;
    }

    private List<EnemySpawnConfig.EnemySpawningConfig> SpawnableEnemies
    {
        get
        {
            return _config.Enemies
                .Where(enemy => !_existingEnemies.ContainsKey(enemy.Enemy.Config) 
                              || _existingEnemies[enemy.Enemy.Config] < enemy.MaxAmount)
                .ToList();
        }
    }

    private Transform GetRandomArea()
    {
        return spawnAreas[Random.Range(0, spawnAreas.Length)];
    }

    private Vector2 GetAreaPosition(Transform area)
    {
        Vector2 retValue = area.position;
        var randomSpawnRadius = _config.RandomSpawnRadius;
        retValue += new Vector2(Random.Range(-randomSpawnRadius, randomSpawnRadius), Random.Range(-randomSpawnRadius, randomSpawnRadius));
        return retValue;
    }

    private Quaternion GetAreaRotation(Transform area)
    {
        Quaternion retValue = area.rotation;
        var randomSpawnRotation = _config.RandomSpawnRotation;
        retValue *= Quaternion.Euler(0, 0, Random.Range(-randomSpawnRotation, randomSpawnRotation));
        return retValue;
    }
    
}
