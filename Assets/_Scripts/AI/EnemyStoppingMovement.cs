﻿using UnityEngine;

public class EnemyStoppingMovement : EnemyMovement
{

    protected override void FixedUpdate()
    {
        if (CurrentPlayer == null) return;

        Vector2 position = gameObject.transform.position;
        Vector2 targetPosition = CurrentPlayer.position;

        Rotate(targetPosition - position);

        if ((targetPosition - position).magnitude < config.StoppingDistance)
        {
            SlowDown();
            RotateToLookAt();
        }
        else
        {
            SetupMovementPattern();
            AccelerateTowardsTarget(position, targetPosition);
        }

        if (currentSpeed == 0) return;

        movementPattern.SetMovementDirection(transform.up, transform.right);
        movementPattern.Move(transform, currentSpeed, Time.fixedDeltaTime);
    }

}