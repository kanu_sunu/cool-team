﻿using UnityEngine;

public class EnemyConstantMovement : EnemyMovement
{
    protected override void FixedUpdate()
    {
        if (CurrentPlayer == null) return;

        Vector2 position = gameObject.transform.position;
        Vector2 targetPosition = CurrentPlayer.position;

        if ((targetPosition - position).magnitude < config.StoppingDistance)
        {
            Rotate(transform.up);

            if (currentSpeed < config.MaxMovementSpeed / 2)
            {
                AccelerateTowardsTarget(position, position + (Vector2)transform.up);
            }
        }
        else
        {
            Rotate(targetPosition - position);

            SetupMovementPattern();
            AccelerateTowardsTarget(position, targetPosition);
        }

        if (currentSpeed == 0) return;

        movementPattern.SetMovementDirection(transform.up, transform.right);
        movementPattern.Move(transform, currentSpeed, Time.fixedDeltaTime);
    }
}