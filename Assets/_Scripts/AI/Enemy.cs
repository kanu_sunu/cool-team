using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using _Scripts.Data;
using _Scripts.Pickups;

namespace _Scripts.AI
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _shield;
        [SerializeField] private Health _health;
        [SerializeField] private EnemyConfig _config;

        private DropItemOnDeath _dropsWithoutShield;
        private DropItemOnDeath _dropsWithShield;

        public Health Health { get { return _health; } }
        public EnemyConfig Config { get { return _config; } }

        private void Awake()
        {
            _dropsWithoutShield = new DropItemOnDeath(Health, Config.DropsWithoutShield, transform).AddTo(gameObject);
        }

        public void ActivateShield(PlayerId player)
        {
            Health.Shield = player;
            _shield.sprite = GameConfig.Player(player).Shield;
            _shield.gameObject.SetActive(true);
            
            _dropsWithoutShield.Dispose();
            _dropsWithoutShield = null;
            
            if (_dropsWithShield != null)
                _dropsWithShield.Dispose();
            _dropsWithShield = new DropItemOnDeath(Health, Config.DropsWithShield, transform).AddTo(gameObject);
        }
    }
}