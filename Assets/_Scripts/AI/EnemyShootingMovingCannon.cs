﻿using _Scripts;
using UnityEngine;

public class EnemyShootingMovingCannon : EnemyShooting
{
    protected override void FixedUpdate()
    {
        if (movement.CurrentPlayer == null) return;

        Vector2 position = gameObject.transform.position;
        Vector2 targetPosition = movement.CurrentPlayer.position;

        Vector2 shootingDirection = (targetPosition - position);

        weaponShooter.ShootTowards(shootingDirection);

        if (shootingDirection.magnitude < movement.Config.StoppingDistance)
        {
            if (Vector3.Angle(shootingDirection, gameObject.transform.up) > shootingAngle) return;

            SendShootingRequest(true);
        }
        else
        {
            SendShootingRequest(false);
        }
    }
}