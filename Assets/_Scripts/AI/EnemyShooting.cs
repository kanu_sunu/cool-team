﻿using _Scripts;
using UnityEngine;

public abstract class EnemyShooting : MonoBehaviour
{
    [SerializeField] protected float shootingAngle = 15;
    [Space]
    [SerializeField] protected EnemyMovement movement;
    [SerializeField] protected WeaponShooter weaponShooter;
    [SerializeField] protected Rigidbody2D rigid;

    private bool isShooting = false;

    protected abstract void FixedUpdate();

    protected void SendShootingRequest(bool startShooting)
    {
        if (isShooting == startShooting) return;

        ShootRequest newShootRequest = new ShootRequest()
        {
            StartShooting = startShooting,
            Rigidbody = rigid
        };

        weaponShooter.RequestShooting(newShootRequest);

        isShooting = startShooting;
    }

}
