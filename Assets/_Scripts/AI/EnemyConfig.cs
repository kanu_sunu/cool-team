﻿using System;
using System.Collections.Generic;
using UnityEngine;
using _Scripts.Pickups;

[CreateAssetMenu(fileName = "EnemyConfig", menuName = "Config/Enemy")]
public class EnemyConfig : ScriptableObject
{
    [Serializable]
    public class DropChance
    {
        public AbstractPickupConfig Pickup;
        public float Chance;
    }

    [SerializeField] private float targetUpdateInterval;
    [SerializeField] private float stoppingDistance;
    [Space]
    [SerializeField] private float accelerationSpeed;
    [SerializeField] private float maxMovementSpeed;
    [SerializeField] private float slowDownSpeed;
    [Space]
    [SerializeField] private float rotationSpeed;

    [Space] 
    [SerializeField] private List<DropChance> _dropsWithoutShield;
    [SerializeField] private List<DropChance> _dropsWithShield;

    public float TargetUpdateInterval { get { return targetUpdateInterval; } }
    public float StoppingDistance { get { return stoppingDistance; } }
    public float AccelerationSpeed { get { return accelerationSpeed; } }
    public float MaxMovementSpeed { get { return maxMovementSpeed; } }
    public float SlowDownSpeed { get { return slowDownSpeed; } }
    public float RotationSpeed { get { return rotationSpeed; } }
    public List<DropChance> DropsWithoutShield { get { return _dropsWithoutShield; } }
    public List<DropChance> DropsWithShield { get { return _dropsWithShield; } }
}