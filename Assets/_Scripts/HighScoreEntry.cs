using System;
using TMPro;
using UnityEngine;

namespace _Scripts
{
    public class HighScoreEntry : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _rank;
        [SerializeField] private TextMeshProUGUI _name;
        [SerializeField] private TextMeshProUGUI _time;

        public int Rank
        {
            set { _rank.text = value.ToString(); }
        }

        public string Name
        {
            set { _name.text = value; }
        }
        
        public TimeSpan Time
        {
            set { _time.text = string.Format("{0:D2}:{1:D2}", value.Minutes, value.Seconds); }
        }
    }
}