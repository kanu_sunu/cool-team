﻿using _Scripts;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class BobaFettExplosion : MonoBehaviour {
    
    [SerializeField] private float lifeTime;
    
    private void Start()
    {
        Destroy(gameObject, lifeTime);
    }
}
