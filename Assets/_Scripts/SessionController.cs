using System;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using _Scripts.Data;

namespace _Scripts
{
    public class SessionController : MonoBehaviour
    {
        private const float TickInterval = 0.1f;

        public readonly ReactiveProperty<TimeSpan> SessionTime = new ReactiveProperty<TimeSpan>();
        private readonly SerialDisposable _timerDisposer = new SerialDisposable();
        
        private readonly ReactiveProperty<bool> _gameOver = new ReactiveProperty<bool>(false);
        public IReadOnlyReactiveProperty<bool> GameOver
        {
            get { return _gameOver; }
        }
        
        private readonly ReactiveProperty<bool> _gameIsStopped = new ReactiveProperty<bool>(false);
        public IReadOnlyReactiveProperty<bool> GameIsStopped
        {
            get { return _gameIsStopped; }
        }
        
        private readonly Subject<Unit> _gameRestart = new Subject<Unit>();
        public Subject<Unit> GameRestart
        {
            get { return _gameRestart; }
        }

        private readonly CompositeDisposable _playerInputDisposer = new CompositeDisposable();

        private void Start()
        {
            StartTimer();
            GameConfig.Instance.Players.ForEach(player =>
            {
                player.Health.IsAlive
                    .Where(isAlive => !isAlive)
                    .Subscribe(_ => EndGame())
                    .AddTo(player);

                player.StartPressed
                    .Where(_ => _gameOver.Value)
                    .Subscribe(_ => ResetGame())
                    .AddTo(_playerInputDisposer);
            });
        }

        private void StartTimer()
        {
            _timerDisposer.Disposable = Observable.Interval(TimeSpan.FromSeconds(TickInterval))
                .Subscribe(ticks => SessionTime.Value = SessionTime.Value.Add(TimeSpan.FromSeconds(TickInterval)));
        }
        
        private void EndGame()
        {
            StopTimer();
            _gameOver.Value = true;
            SlowDown();
        }

        private void SlowDown()
        {
            Observable.Interval(TimeSpan.FromMilliseconds(15))
                .TakeWhile(_ => Time.timeScale > 0.2)
                .DoOnCompleted(() =>
                {
                    Time.timeScale = 0;
                    _gameIsStopped.Value = true;
                })
                .Subscribe(amount => Time.timeScale = Math.Max(0, Time.timeScale - 0.01f))
                .AddTo(gameObject);
        }

        public void ResetGame()
        {
            _playerInputDisposer.Dispose();
            _gameRestart.OnNext(Unit.Default);
            SceneManager.UnloadSceneAsync(0);
            SceneManager.LoadScene(0);
            Time.timeScale = 1;
        }

        private void StopTimer()
        {
            _timerDisposer.Disposable.Dispose();
        }
    }
}