﻿using UnityEngine;
using UniRx;
using System;
using _Scripts.Data;
using System.Linq;
using _Scripts;
using System.Collections.Generic;
using _Scripts.Audio;

public class SafeZone : MonoBehaviour
{

    [SerializeField] private float safeRange;
    [SerializeField] private float warningDistance;
    [Space]
    [SerializeField] private int damageIncrease;
    [SerializeField] private float damageInterval;
    [SerializeField] private AudioClip _warningClip;
    [SerializeField] private AudioPlayer _warningAudioPlayer;

    private List<Player> players;
    private int[] outSideSeconds;

    public float SafeRange { get { return safeRange; } }
    
    private readonly ReactiveProperty<bool> _isPlayerOutsideSafeZone = new ReactiveProperty<bool>();

    private void Start()
    {
        players = GameConfig.Instance.Players;
        outSideSeconds = new int[players.Count];

        var source = Observable.Interval(TimeSpan.FromSeconds(damageInterval));
        source.Subscribe(_ => CheckForOutsidePlayers());

        _warningAudioPlayer.SetClip(_warningClip);
        Observable.Interval(TimeSpan.FromSeconds(0.5f))
            .Where(_ => _isPlayerOutsideSafeZone.Value)
            .Subscribe(_ => _warningAudioPlayer.Play());

        _isPlayerOutsideSafeZone.Subscribe(val => Debug.Log(val));
    }
    
    private void CheckForOutsidePlayers()
    {
        var anyPlayerOutsideSafeZone = false;
        for (int i = 0; i < players.Count; i++)
        {
            var player = players[i];
            if (player == null) continue;

            int seconds = 0;
            float distance = (player.transform.position - gameObject.transform.position).magnitude;
            if (distance > safeRange)
            {
                outSideSeconds[i] += 1;
                seconds = outSideSeconds[i];

                player.WarningTrangle.EnterZone();
            }
            else
            {
                outSideSeconds[i] = 0;

                player.WarningTrangle.ExitZone();
            }
            
            if (distance > safeRange - warningDistance)
            {
                player.WarningTrangle.Enable();
                anyPlayerOutsideSafeZone = true;
            }
            else
            {
                player.WarningTrangle.Disable();
            }

            if (seconds > 0)
            {
                player.Health.ChangeHealth(seconds * -damageIncrease);
            }
        }

        _isPlayerOutsideSafeZone.Value = anyPlayerOutsideSafeZone;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(gameObject.transform.position, safeRange);
    }

}