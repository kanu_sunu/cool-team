using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using _Scripts.Data;

namespace _Scripts
{
    public class InitUiController : MonoBehaviour
    {
        [SerializeField] private GameObject _soloSelection;
        [SerializeField] private GameObject _coopSelection;

        private void Start()
        {
            GameConfig.Input(PlayerId.One).A.Where(x => x)
                .Merge(GameConfig.Input(PlayerId.Two).A.Where(x => x))
                .Take(1)
                .Subscribe(_ => StartGame());

            GameConfig.Input(PlayerId.One).Horizontal.Where(value => value > 0)
                .Merge(GameConfig.Input(PlayerId.Two).Horizontal.Where(value => value > 0))
                .Subscribe(_ => SelectCoop()).AddTo(gameObject);

            GameConfig.Input(PlayerId.One).Horizontal.Where(value => value < 0)
                .Merge(GameConfig.Input(PlayerId.Two).Horizontal.Where(value => value < 0))
                .Subscribe(_ => SelectSolo()).AddTo(gameObject);
        }

        private void SelectCoop()
        {
            GameConfig.Coop = true;
            _soloSelection.SetActive(false);
            _coopSelection.SetActive(true);
        }

        private void SelectSolo()
        {
            GameConfig.Coop = false;
            _soloSelection.SetActive(true);
            _coopSelection.SetActive(false);
        }

        private static void StartGame()
        {
            SceneManager.LoadSceneAsync("Game", LoadSceneMode.Single);
        }
    }
}