﻿using UniRx;
using UnityEngine;
using _Scripts.Audio;

namespace _Scripts
{
    public class Health : MonoBehaviour
    {
        [SerializeField] private float invincibilityTime = 0.1f;
        [SerializeField] private ActorType _actor;
        public IntReactiveProperty MaxHealth = new IntReactiveProperty();
        [HideInInspector] public IntReactiveProperty CurrentHealth = new IntReactiveProperty();

        private readonly ReactiveProperty<bool> _isInvincible = new ReactiveProperty<bool>(false);

        public IReadOnlyReactiveProperty<bool> IsInvincible
        {
            get { return _isInvincible; }
        }

        private readonly ReactiveProperty<bool> _isAlive = new ReactiveProperty<bool>(true);

        public IReadOnlyReactiveProperty<bool> IsAlive
        {
            get { return _isAlive; }
        }

        public PlayerId? Shield;

        public void ChangeHealth(int byAmount)
        {
            if (!IsAlive.Value) return;
            if (IsInvincible.Value) return;

            CurrentHealth.Value = Mathf.Clamp(CurrentHealth.Value + byAmount, 0, MaxHealth.Value);

            if (CurrentHealth.Value <= 0)
            {
                _isAlive.Value = false;
                Destroy(gameObject);
            }
            else if (_actor == ActorType.Player && byAmount < 0)
            {
                _isInvincible.Value = true;
                Invoke("UndoInvincibility", invincibilityTime);
            }
        }

        private void UndoInvincibility()
        {
            _isInvincible.Value = false;
        }

        private void OnCollision(Collider2D other)
        {
            Damager damager = other.GetComponent<Damager>();
            if (damager != null && damager.ActorType != _actor && (Shield == null || Shield == damager.Player))
            {
                ChangeHealth(-damager.Damage);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            OnCollision(other);
        }

        private void Start()
        {
            CurrentHealth.Value = MaxHealth.Value;
        }

        public ActorType ActorType
        {
            get { return _actor; }
        }
    }
}