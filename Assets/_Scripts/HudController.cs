﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UniRx;
using UnityEngine;
using _Scripts.Data;
using UnityEngine.UI;

namespace _Scripts
{
    public class HudController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _timerText;
        [SerializeField] private List<HealthBar> _healthBars;
        [SerializeField] private List<TextMeshProUGUI> _healthTexts;
        [SerializeField] private List<WarningTriangle> _warningTriangles;
        [SerializeField] private CanvasGroup _endScreenFader;
        [SerializeField] private TextMeshProUGUI _endScreenTimeText;
        [SerializeField] private GameObject _healthBarPlayerTwo;
        private SessionController _sessionController;

        protected void Start()
        {
            _sessionController = GameConfig.Instance.SessionController;
            _sessionController.SessionTime
                .Subscribe(time => _timerText.text = GetTimerText(time));

            _sessionController.GameOver.Subscribe(_ => ShowEndScreen()).AddTo(gameObject);
                
            
            var players = GameConfig.Instance.Players;
            for (var idx = 0; idx < players.Count; idx++)
            {
                SetupHealthBar(players[idx].Health, _healthBars[idx]);
                SetupHealthText(players[idx].Health, _healthTexts[idx]);
                SetupWarningTriangle(players[idx], _warningTriangles[idx]);
            }
            
            if (!GameConfig.Coop)
                _healthBarPlayerTwo.SetActive(false);

            HideEndScreen(false);
        }

        private void SetupWarningTriangle(Player player, WarningTriangle triangle)
        {
            player.WarningTrangle = triangle;
        }

        private void SetupHealthText(Health health, TextMeshProUGUI healthText)
        {
            health.CurrentHealth
                .CombineLatest(health.MaxHealth, (current, max) => new {Current = current, Max = max})
                .Subscribe(healthTuple =>
                {
                    healthText.text = string.Format("{0} / {1}", healthTuple.Current, healthTuple.Max);
                });
        }

        private void SetupHealthBar(Health health, HealthBar healthBar)
        {
            health.CurrentHealth
                .CombineLatest(health.MaxHealth,
                    (current, max) => (float) current / (float) max)
                .Subscribe(val => healthBar.Value = val);
        }

        private void ShowEndScreen()
        {
            _endScreenFader.gameObject.SetActive(true);
            _endScreenTimeText.text = GetTimerText(_sessionController.SessionTime.Value);
            _endScreenFader.DOFade(1f, 0.4f);
        }
        
        private void HideEndScreen(bool animate = true)
        {
            _endScreenFader.DOFade(0f, animate ? 0.4f : 0f)
                .OnComplete(() => _endScreenFader.gameObject.SetActive(false));
        }
        
        private static string GetTimerText(TimeSpan time)
        {
            return string.Format("{0:D2}:{1:D2}", time.Minutes, time.Seconds);
        }
    }
}