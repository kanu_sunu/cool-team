﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using _Scripts;
using _Scripts.Data;

public class Zoomer : MonoBehaviour
{
	public float m_DampTime = 0.2f;                 // Approximate time for the camera to refocus.
    public float m_ScreenEdgeBuffer = 4f;           // Space between the top/bottom most target and the screen edge.
    public float m_MinSize = 6.5f;                  // The smallest orthographic size the camera can be.
    private List<Transform> m_Targets; // All the targets the camera needs to encompass.

    [SerializeField] private Camera m_Camera;                        // Used for referencing the camera.
    private float m_ZoomSpeed;                      // Reference speed for the smooth damping of the orthographic size.
    private Vector3 m_MoveVelocity;                 // Reference velocity for the smooth damping of the position.
    private Vector3 m_DesiredPosition;              // The position the camera is moving towards.

    private void Start ()
    {
        m_Targets = GameConfig.Instance.Players.Select(player => player.transform).ToList();
    }

    private void FixedUpdate ()
    {
        m_Targets.RemoveAll(t => !t);
        if (m_Targets.Count <= 0)
            return;
        
        Move ();
        Zoom ();
    }

    private void Move ()
    {
        FindAveragePosition ();
        transform.position = Vector3.SmoothDamp(transform.position, m_DesiredPosition, ref m_MoveVelocity, m_DampTime);
    }

    private void FindAveragePosition ()
    {
        var averagePos = new Vector3 ();
        var numTargets = 0;

        foreach (var t in m_Targets)
        {
            if (!t.gameObject.activeSelf)
                continue;

            averagePos += t.position;
            numTargets++;
        }

        if (numTargets > 0)
            averagePos /= numTargets;

        averagePos.z = transform.position.z;

        m_DesiredPosition = averagePos;
    }

    private void Zoom ()
    {
        m_Camera.orthographicSize = Mathf.SmoothDamp (m_Camera.orthographicSize, FindRequiredSize(), ref m_ZoomSpeed, m_DampTime);
    }

    private float FindRequiredSize ()
    {
        var desiredLocalPos = transform.InverseTransformPoint(m_DesiredPosition);

        var size = 0f;

        foreach (var t in m_Targets)
        {
            if (!t.gameObject.activeSelf)
                continue;

            var targetLocalPos = transform.InverseTransformPoint(t.position);
            var desiredPosToTarget = targetLocalPos - desiredLocalPos;
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / m_Camera.aspect);
        }

        size += m_ScreenEdgeBuffer;

        return Mathf.Max (size, m_MinSize);
    }

    public void SetStartPositionAndSize ()
    {
        FindAveragePosition ();
        transform.position = m_DesiredPosition;
        m_Camera.orthographicSize = FindRequiredSize ();
    }
	
}
