﻿using UniRx;
using UnityEngine;
using _Scripts;
using _Scripts.Data;

public class WeaponRotation : MonoBehaviour
{
	[SerializeField] private Player _player;
	[SerializeField] private Transform _weapon;
	[SerializeField] private float _rotationSpeed = 15;

	private void Start ()
	{
		GameConfig.Input(_player.PlayerId).Horizontal
			.TakeWhile(_ => !GameConfig.Instance.SessionController.GameIsStopped.Value)
			.CombineLatest(GameConfig.Input(_player.PlayerId).Vertical, CalculateRotation).Subscribe(RotatePlayer).AddTo(_weapon);
	}

	private Quaternion CalculateRotation(float horizontal, float vertical)
	{
		var direction = new Vector3(horizontal, vertical, 0);
		var angle = Vector3.SignedAngle(Vector3.up, direction, Vector3.forward);
		return Quaternion.AngleAxis(angle, Vector3.forward);
	}

	private void RotatePlayer(Quaternion rotation)
	{
		_weapon.rotation = Quaternion.RotateTowards(_weapon.rotation, rotation, _rotationSpeed);
	}
}
