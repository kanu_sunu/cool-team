﻿using UnityEngine;

public class MissileBullet : EffectBullet
{
    [SerializeField] private GameObject bulletPref;
    [SerializeField] private float bulletSpeed;

    protected override void OnDestruction()
    {
        float randomOffset = Random.Range(0, 90);

        for (int i = 0; i < 4; i++)
        {
            float currentRotation = i * 90 + randomOffset;

            GameObject bulletObj = Instantiate(bulletPref, transform.position, Quaternion.Euler(new Vector3(0, 0, currentRotation)));
            bulletObj.GetComponent<Bullet>().SetVelocity(bulletObj.transform.up * bulletSpeed);
        }
    }
}
