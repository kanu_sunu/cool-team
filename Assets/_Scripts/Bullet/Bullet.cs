﻿using System;
using UniRx;
using UnityEngine;
using _Scripts;

public class Bullet : MonoBehaviour
{
	protected Rigidbody2D Rigidbody
	{
		get { return _rigidbody; }
	}

	public Vector2 Direction
	{
		get { return _rigidbody ? _rigidbody.velocity.normalized : Vector2.zero; }
	} 

	[SerializeField] private Rigidbody2D _rigidbody;
	[SerializeField] private float _lifeSpan;
	[SerializeField] private SpriteRenderer _sprite;
	[SerializeField] protected Damager _damager;
	
	private readonly Subject<Unit> _destruction = new Subject<Unit>();
	public IObservable<Unit> Destruction
	{
		get { return _destruction; }
	}
	
	private ActorType _actor;

	private void Start()
	{
		Observable.Timer(TimeSpan.FromSeconds(_lifeSpan)).Subscribe(_ => Destroy(gameObject)).AddTo(gameObject);
	}

	public virtual void SetSprite(Sprite sprite, Color tint)
	{
		_sprite.sprite = sprite;
		_sprite.color = tint;
	}

	public virtual void SetVelocity(Vector2 velocity)
	{
        Rigidbody.velocity = velocity;
	}

	public virtual void SetDamage(int damage, ActorType actor, PlayerId? player)
	{
		_actor = actor;
		_damager.Setup(damage, actor, player);
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		var health = other.GetComponent<Health>();
		if (health != null && health.ActorType != _actor)
		{
			_destruction.OnNext(Unit.Default);
			Destroy(gameObject);
		}
	}
}
