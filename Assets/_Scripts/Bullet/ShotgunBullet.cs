using System.Collections.Generic;
using UnityEngine;

namespace _Scripts
{
    public class ShotgunBullet : Bullet
    {
        [SerializeField] private List<Bullet> _bullets;
        
        public override void SetSprite(Sprite sprite, Color tint)
        {
            _bullets.ForEach(bullet => bullet.SetSprite(sprite, tint));
        }

        public override void SetVelocity(Vector2 velocity)
        {
            var difference = 90f / _bullets.Count;
            for (var i = 0; i < _bullets.Count; i++)
            {
                _bullets[i].SetVelocity(Rotate(velocity, -45 + i * difference));
            }
        }
        
        private static Vector2 Rotate(Vector2 v, float degrees) {
            var radians = degrees * Mathf.Deg2Rad;
            var sin = Mathf.Sin(radians);
            var cos = Mathf.Cos(radians);
         
            var tx = v.x;
            var ty = v.y;
 
            return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
        }

        public override void SetDamage(int damage, ActorType actor, PlayerId? player)
        {
            _bullets.ForEach(bullet => bullet.SetDamage(damage, actor, player));
        }
    }
}