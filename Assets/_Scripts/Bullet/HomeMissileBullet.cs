﻿using UnityEngine;

public class HomeMissileBullet : EffectBullet
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private float rotateSpeed = 200f;

    private Transform target;
    
    private void Update()
    {
        Vector2 direction = (Vector2)target.position - Rigidbody.position;
        direction.Normalize();

        float rotateAmount = Vector3.Cross(direction, transform.up).z;

        Rigidbody.angularVelocity = -rotateAmount * rotateSpeed;

        Rigidbody.velocity = transform.up * speed;
    }

    public void Setup(Transform p_target)
    {
        target = p_target;
    }

    protected override void OnDestruction()
    {
        
    }
}
