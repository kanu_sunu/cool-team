﻿using UnityEngine;

public class BobaFettMine : EffectBullet
{
    [SerializeField] private GameObject destroyAnimationPref;

    private Damager destroyAnimationObj;

    protected override void OnDestruction()
    {
        if (destroyAnimationObj != null) return;

        destroyAnimationObj = Instantiate(destroyAnimationPref, transform.position, transform.rotation).GetComponentInChildren<Damager>();
        destroyAnimationObj.Setup(_damager.Damage, _damager.ActorType, _damager.Player);
    }
}
