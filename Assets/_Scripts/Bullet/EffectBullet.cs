﻿public abstract class EffectBullet : Bullet
{    
    protected abstract void OnDestruction();

    private void OnDestroy()
    {
        OnDestruction();
    }

}