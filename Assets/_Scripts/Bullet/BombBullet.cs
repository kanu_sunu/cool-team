﻿using UnityEngine;

public class BombBullet : EffectBullet
{
    [SerializeField] private int damage = 5;
    [SerializeField] private float brakeInSeconds = 2.0f;

    private void Start()
    {
        Invoke("Brake", brakeInSeconds);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);

        //damage player logic
    }

    private void Brake()
    {
        Rigidbody.drag = 5;
    }

    protected override void OnDestruction()
    {

    }
}
